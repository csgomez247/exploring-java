/**`
 * UsingIteratorPastEndException.java
 */
public class UsingIteratorPastEndException extends Exception {
	
	static final long serialVersionUID = 98L;

	/**
	 * UsingIteratorPastEndException Constructor
	 * @param  msg error message
	 */
	public UsingIteratorPastEndException(String msg) {
		super(msg);
	}//Constructor (String msg)

}//UsingIteratorPastEndException
