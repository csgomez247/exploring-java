/**
 * Plus.java
 */
public class Plus {

	/**
	 * The plus method to end all plus methods
	 * @param  Seq 'top' sequence
	 * @param  Seq 'bottom' sequence
	 * @return    The best kind of sequence for the sum
	 */		
	public static Seq plus(Seq a, Seq b) {

		SeqIt aitr = a.createSeqIt();
		SeqIt bitr = b.createSeqIt();

		int aLength = getLength(a);
		int bLength = getLength(b);

		int initial = 0;
		int current = 0, next = 0;
		int currDelta = 0, nextDelta = 0;

		boolean isConstant = true;
		boolean isDelta = true;
		boolean isJumble = true;

		try {

			if(aitr.hasNext() && bitr.hasNext())
				current = aitr.next() + bitr.next();
			else
				return new Constant(0,0);

			if(aitr.hasNext() && bitr.hasNext())
				next = aitr.next() + bitr.next();
			else
				return new Constant(1, current);

			initial = current;

			if(current != next)
				isConstant = false;

			currDelta = next - current;

			current = next;

			for(int i = 2; i < Math.min(aLength,bLength); i++) {

				next = aitr.next() + bitr.next();

				if(current != next)
					isConstant = false;

				nextDelta = next - current;

				if(currDelta != nextDelta)
					isDelta = false;

				current = next;
				currDelta = nextDelta;
			}//for

			if(isConstant)
				return new Constant(Math.min(aLength,bLength), initial);

			else if(isDelta)
				return new Delta(Math.min(aLength,bLength), initial, currDelta);

			else {
				aitr = a.createSeqIt();
				bitr = b.createSeqIt();

				int [] j = new int [Math.min(aLength,bLength)];

				for(int i = 0; i < j.length; i++) {
					j[i] = aitr.next() + bitr.next();
				}//for

				return new Jumble(j);
			}//else

		}//try
		catch(UsingIteratorPastEndException e) {
			e.printStackTrace();
		}

		return new Constant(1,1);
	}//plus

	private static int getLength(Seq s) {
		SeqIt sitr = s.createSeqIt();
		int count = 0;

		try {
			while(sitr.hasNext()) {
				sitr.next();
				count++;
			}
		}
		catch(UsingIteratorPastEndException e) {
			e.printStackTrace();
		}
		return count;
	}//getLength

}//Plus