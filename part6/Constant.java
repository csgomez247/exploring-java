/**
 * Constant.java
 */
public class Constant extends Seq {

	protected int num;
	protected int value;

	/**
	 * Construct Contant object
	 * @param n number of values
	 * @param v value of each element
	 */
	public Constant(int n, int v) {

		this.num = n;

		if(n == 0)
			this.value = 0;
		else
			this.value = v;	

	}//constructor(int n, int v)

	/**
	 * Creates an iterator for Constant
	 * @return ConstantIt
	 */
	public SeqIt createSeqIt() {
		return new ConstantIt(this);
	}//createSeqIt

	/**
	 * Computes the minimum value in a Constant sequence
	 * @return minimum value in sequence
	 */
	public int min() {

		if(this.num == 0)
			return 0;
		return this.value;
		
	}//min

	/**
	 * Stringifies a Constant
	 * @return stringified Constant
	 */
	@Override
	public String toString() {

		return "[ " + this.num + " : " + this.value + " ]";

	}//toString()

}//Constant