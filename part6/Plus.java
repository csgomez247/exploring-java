/**
 * Plus.java
 */
public class Plus {

	/**
	 * Adds two Constant sequences together
	 * @param  a 'top' sequence
	 * @param  b 'bottom' sequence
	 * @return   a sequence resembling a + b
	 */
	public static Seq plus(Constant a, Constant b) {

		return new Constant(Math.min(a.num, b.num), a.value + b.value);
		
	}//plus

	/**
	 * Adds two Delta sequences together
	 * @param  a 'top' sequence
	 * @param  b 'bottom' sequence
	 * @return   a sequence resembling a + b
	 */
	public static Seq plus(Delta a, Delta b) {

		return new Delta(Math.min(a.num, b.num), a.initial + b.initial, a.delta + b.delta);

	}//plus

	/**
	 * Adds two Jumble sequences together
	 * @param  a 'top' sequence
	 * @param  b 'bottom' sequence
	 * @return   a sequence resembling a + b
	 */
	public static Seq plus(Jumble a, Jumble b) {
		
		int [] j = new int [Math.min(a.values.length, b.values.length)];

		for(int i = 0; i < Math.min(a.values.length, b.values.length); i++)
			j[i] =  a.values[i] + b.values[i];

		return new Jumble(j);

	}//plus

}//Plus