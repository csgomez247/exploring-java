/**
 * ConstantIt.java
 */
public class ConstantIt implements SeqIt{
	
	private int num; // number of elements
	private int value; // constant value

	private int currNum; // current element number, 1 based

	/**
	 * Constructs a ConstantIt iterator
	 * @param  s Constant to be iterated
	 */
	public ConstantIt(Constant s) {
		
		this.num = s.num;
		this.value = s.value;
		this.currNum = 0;
	
	}//Constructor (Constant s)

	/**
	 * Decides if the array has more elements
	 * @return if the array has a next element
	 */
	public boolean hasNext() {

		return currNum < this.num;
	
	}//hasNext

	/**
	 * Returns the next element in sequence
	 * @throws UsingIteratorPastEndException ConstantIt called past end
	 * @return the next element
	 */
	public int next() throws UsingIteratorPastEndException {

		// if there is no next element
		if(hasNext() == false) {
			//System.err.println("ConstantIt called past end");
			throw new UsingIteratorPastEndException("ConstantIt called past end");
			//System.exit(1);
		}//if
		currNum++;
		return value;
	
	}//next
}