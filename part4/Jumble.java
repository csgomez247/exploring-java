/**
 * Jumble.java
 */
public class Jumble extends Seq{

	protected int [] values;

	/**
	 * Constructs a Jumble that takes an array arugment
	 * @param  v array of integers with no restrictions
	 */
	public Jumble(int [] v) {
		
		values = new int[v.length];
		System.arraycopy(v, 0, values, 0, v.length);
	
	}// Constructor, 1 arg

	/**
	 * Computes the minimum value in a Jumble sequence
	 * @return minimum value in sequence
	 */
	public int min() {
		if(this.values.length == 0)
			return 0;
		int currentMin = this.values[0];
		for(int i = 0; i < this.values.length; i++)
			currentMin = Math.min(currentMin, this.values[i]);
		return currentMin;
	}//min

	/**
	 * Strinifies Jumble
	 * @return stringified Jumble
	 */
	@Override
	public String toString() {

		String elements = "";

		for(int i = 0; i < this.values.length; i++) {
			elements = elements.concat(this.values[i] + " ");
		}//loop through elements

		return "{ " + this.values.length + " : " + elements + "}";

	}//toString

}//Jumble