/**
 * DeltaIt.java
 */
public class DeltaIt implements SeqIt {

	private int num; // number of elements
	private int currVal; // the current element value
	private int delta; // constant change between elements

	private int currNum; // current element number, 1 based

	/**
	 * Constructs an Iterator for Delta
	 * @param  s Delta to be traversed
	 * @return   [description]
	 */
	public DeltaIt(Delta s) {

		this.num = s.num;

		/*
		initialized such that the first next() call will
		retrieve the first element
		 */
		this.currVal = s.initial - s.delta; 
		
		this.delta = s.delta;
		this.currNum = 0;

	}//Constructor (Delta s)

	/**
	 * Checks if the sequence has a next element
	 * @return if the sequence has more elements
	 */
	public boolean hasNext() {

		return currNum < this.num;
	
	}//hasNext

	/**
	 * Gets the next element in the sequence
	 * @return the next element
	 */
	public int next() throws UsingIteratorPastEndException {

		// doesn't have a next element
		if(!hasNext()){
			//System.err.println("DeltaIt called past end");
			throw new UsingIteratorPastEndException("DeltaIt called past end");
			//System.exit(1);
		}//if

		this.currVal += this.delta;
		this.currNum++;

		return currVal;

	}//next

}//DeltaIt