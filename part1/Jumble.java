/**
 * Jumble.java
 */
public class Jumble extends Seq{

	protected int [] values;

	/**
	 * Constructs a Jumble that takes an array arugment
	 * @param  v array of integers with no restrictions
	 */
	public Jumble(int [] v) {
		
		values = new int[v.length];
		System.arraycopy(v, 0, values, 0, v.length);
	
	}// Constructor, 1 arg

	/**
	 * Strinifies Jumble
	 * @return stringified Jumble
	 */
	@Override
	public String toString() {

		String elements = "";

		for(int i = 0; i < this.values.length; i++) {
			elements = elements.concat(this.values[i] + " ");
		}//loop through elements

		return "{ " + this.values.length + " : " + elements + "}";

	}//toString

}//Jumble
