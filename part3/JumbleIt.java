/**
 * JumbleIt.java
 */
public class JumbleIt implements SeqIt {

	private int [] values;

	private int currIndex; // 0 based index of values

	/**
	 * Constructs a Jumble iterator
	 * @param  s Jumble to be iterated
	 */
	public JumbleIt(Jumble s) {
		this.values = s.values;
		this.currIndex = -1;
	}//Constructor(Jumble s)

	/**
	 * Checks if the sequence has a next element
	 * @return if the sequence has a next element
	 */
	public boolean hasNext() {
		return this.currIndex < this.values.length - 1;
	}//hasNext

	/**
	 * Gets the next element in the Jumble
	 * @return the next element
	 */
	public int next() {
		
		if(!hasNext()) {
			System.err.println("JumbleIt called past end");
      System.exit(1);
		}//if

		this.currIndex++;
		return this.values[this.currIndex];

	}//next

}//JumbleIt
