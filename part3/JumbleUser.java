/**
 * JumbleUser.java	
 */
public class JumbleUser {

	/**
	 * Finds the longest length of non-decreasing
	 * contiguous subsequence
	 * @param  j Jumble to be analyzed
	 * @return   length of longest NDCSS
	 */
	public static int lengthLongestNDCSS1(Jumble j) {

		JumbleIt jitr = new JumbleIt(j);

		if(!jitr.hasNext())
			return 0;

		int currLength = 1;
		int currLongLength = currLength;
		int currElement = jitr.next(); // the first element
		int nextElement;

		while(jitr.hasNext()) { 

			nextElement = jitr.next();
			
			if(currElement <= nextElement)
				currLength++;
			else
				currLength = 1;

			if(currLength > currLongLength)
				currLongLength = currLength;

			currElement = nextElement;

		}//while

		return currLongLength;

	}//lengthLongestNDCSS1

}//JumbleUser