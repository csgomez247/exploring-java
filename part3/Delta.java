/**
 * Delta.java
 */
public class Delta extends Seq{

	protected int num;
	protected int initial;
	protected int delta;

	/**
	 * Constructs a Delta object with 3 arguments
	 * @param  n number of values
	 * @param  i starting value
	 * @param  d constant difference between values
	 */
	public Delta(int n, int i, int d) {

		this.num = n;

		if(n == 0) {
			this.initial = 0;
			this.delta = 0;
		}//if n == 0
		else {
			this.initial = i;
			this.delta = d;
		}

	}//Constructor, 3 arg

	/**
	 * Computes the minimum value in a Delta sequence
	 * @return minimum value in sequence
	 */
	public int min() {

		if(this.num == 0)
			return 0;
		if(this.delta < 0) {
			int absDelta = this.delta * (this.num - 1);
			if(absDelta < 0)
				absDelta *= -1;
			return this.initial - absDelta;
		}
		return this.initial;
	}//min

	/**
	 * Stringifies a Delta 
	 * @return stringified Delta
	 */
	@Override
	public String toString() {

		return "< " + this.num + " : " + this.initial + " &" + this.delta + " >";

	}//toString()

}//Delta