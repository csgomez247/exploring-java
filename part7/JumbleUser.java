/**
 * JumbleUser.java	
 */
public class JumbleUser {

	/**
	 * Finds the longest length of non-decreasing
	 * contiguous subsequence
	 * @param  j Jumble to be analyzed
	 * @return   length of longest NDCSS
	 */
	public static int lengthLongestNDCSS1(Jumble j) {

		JumbleIt jitr = new JumbleIt(j);
		
		if(!jitr.hasNext())
			return 0;

		int currLength = 1;
		int currLongLength = currLength;
		int currElement = 0;
		
		try {
			currElement = jitr.next(); // the first element
		}
		catch(UsingIteratorPastEndException e) {
			System.out.println("NDCSS1 failed!");
		}//catch

		int nextElement = 0;

		while(jitr.hasNext()) { 
			
			try {
				nextElement = jitr.next();
			}//try
			catch(UsingIteratorPastEndException e) {
				System.out.println("NDCSS1 failed!");
			}//catch
			
			if(currElement <= nextElement)
				currLength++;
			else
				currLength = 1;

			if(currLength > currLongLength)
				currLongLength = currLength;

			currElement = nextElement;

		}//while

		return currLongLength;

	}//lengthLongestNDCSS1

	/**
	 * Finds the longest length of non-decreasing
	 * contiguous subsequence
	 * @param  j Jumble to be analyzed
	 * @return   length of longest NDCSS
	 */
	public static int lengthLongestNDCSS2(Jumble j) {

		JumbleIt jitr = new JumbleIt(j);
		int currLength = 1;
		int currLongLength = 1;
		int currElement = 0;
		int nextElement = 0;

		try {
			currElement = jitr.next();
		}//try
		catch(UsingIteratorPastEndException e) {
			return 0;
		}//catch

		while(true) {

			try {
				nextElement = jitr.next();
			}//try
			catch(UsingIteratorPastEndException e) {
				return currLongLength;
			}//catch
			
			if(currElement <= nextElement)
				currLength++;
			else
				currLength = 1;

			if(currLength > currLongLength)
				currLongLength = currLength;

			currElement = nextElement;

		}//while

	}//lengthLongestNDCSS2

}//JumbleUser