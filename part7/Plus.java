/**
 * Plus.java
 */
public class Plus {

	/**
	 * Adds two Constant sequences together
	 * @param  a 'top' sequence
	 * @param  b 'bottom' sequence
	 * @return   a sequence resembling a + b
	 */
	public static Seq plus(Constant a, Constant b) {

		return new Constant(Math.min(a.num, b.num), a.value + b.value);
		
	}//plus

	/**
	 * Adds two Delta sequences together
	 * @param  a 'top' sequence
	 * @param  b 'bottom' sequence
	 * @return   a sequence resembling a + b
	 */
	public static Seq plus(Delta a, Delta b) {

		return new Delta(Math.min(a.num, b.num), a.initial + b.initial, a.delta + b.delta);

	}//plus

	/**
	 * Adds two Jumble sequences together
	 * @param  a 'top' sequence
	 * @param  b 'bottom' sequence
	 * @return   a sequence resembling a + b
	 */
	public static Seq plus(Jumble a, Jumble b) {
		
		int [] j = new int [Math.min(a.values.length, b.values.length)];

		for(int i = 0; i < j.length; i++)
			j[i] =  a.values[i] + b.values[i];

		return new Jumble(j);

	}//plus

	/**
	 * Adds two sequences (Constant and Delta) together
	 * @param  a 'top' sequence
	 * @param  b 'bottom' sequence
	 * @return   a Delta sequence resembling a + b
	 */
	public static Seq plus(Constant a, Delta b) {

		return new Delta(Math.min(a.num, b.num), a.value + b.initial, b.delta);

	}

	/**
	 * Adds two sequences (Delta and Constant) together
	 * @param  a 'top' sequence
	 * @param  b 'bottom' sequence
	 * @return   a sequence resembling a + b
	 */
	public static Seq plus(Delta a, Constant b) {
		return plus(b,a);
	}

	/**
	 * Adds two sequences (Constant, Jumble) together
	 * @param  a 'top' sequence
	 * @param  b 'bottom' sequence
	 * @return   a sequence resembling a + b
	 */
	public static Seq plus(Constant a, Jumble b) {
		
		int [] j = new int [Math.min(a.num, b.values.length)];

		for(int i = 0; i < j.length; i++)
			j[i] = a.value + b.values[i];

		return new Jumble(j);
	}

	/**
	 * Adds two sequences (Jumble, Constant) together
	 * @param  a 'top' sequence
	 * @param  b 'bottom' sequence
	 * @return   a sequence resembling a + b
	 */
	public static Seq plus(Jumble a, Constant b) {
		return plus(b,a);
	}

	/**
	 * Adds two sequences (Delta, Jumble) together
	 * @param  a 'top' sequence
	 * @param  b 'bottom' sequence
	 * @return   a sequence resembling a + b
	 */
	public static Seq plus(Delta a, Jumble b) {
		
		int [] j = new int [Math.min(a.num, b.values.length)];

		for (int i = 0; i < j.length; i++) {
			if(a.delta >= 0)
				j[i] = a.initial + (i * a.delta) + b.values[i];
		
			else  {// if(a.delta < 0) 
				int absDelta = i * a.delta;
				if(absDelta < 0)
					absDelta *= -1;
				j[i] = a.initial - absDelta + b.values[i];
			}
		}//for

		return new Jumble(j);

	}

	/**
	 * Adds two sequences (Jumble, Delta) together
	 * @param  a 'top' sequence
	 * @param  b 'bottom' sequence
	 * @return   a sequence resembling a + b
	 */
	public static Seq plus(Jumble a, Delta b) {
		return plus(b,a);
	}

}//Plus
